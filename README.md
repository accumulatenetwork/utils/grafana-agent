# Grafana Agent

**Note:** The firewall in older versions of AccMan blocks the grafana container from scraping the node's Prometheus metrics. The node exporter will still work, but host port 16594 must be reachable from within the grafana container or node metrics will not be scraped.

1. `git clone https://gitlab.com/accumulatenetwork/utils/grafana-agent.git`
2. `cd grafana-agent`
3. Create `.env`:

       REMOTE_WRITE_URL=<grafana push URL>
       REMOTE_WRITE_USERNAME=<grafana user ID>
       REMOTE_WRITE_PASSWORD=<grafana secret>

4. `./start.sh`

## AWS hostname

Execute the following steps to update the hostname of an AWS instance. Do this before installing the agent, or run `./start.sh` after to update the agent's configuration with the new hostname.

1. Open `/etc/cloud/cloud.cfg` in an editor.
2. Set `preserve_hostname: true` so that hostname changes are preserved.
3. Close the editor.
4. Execute `hostnamectl set-hostname <desired hostname>` to set the instance's hostname.
