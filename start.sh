#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if ! [ -f "$SCRIPT_DIR/.env" ]; then
  >&2 echo "Missing $SCRIPT_DIR/.env"
  exit 1
fi

source "$SCRIPT_DIR/.env"

mkdir -p /etc/grafana/data
ln -sf "$SCRIPT_DIR/agent.yaml" /etc/grafana/agent.yaml

docker stop grafana-agent
docker rm grafana-agent
docker run --name grafana-agent \
  -d --restart unless-stopped \
  --hostname `hostname` \
  --add-host host.docker.internal:host-gateway \
  -v /etc/grafana/data:/etc/agent/data \
  -v /etc/grafana/agent.yaml:/etc/agent/agent.yaml \
  ${DOCKER_ENV_VARS} \
  grafana/agent:v0.37.2 \
  -config.file /etc/agent/agent.yaml \
  -config.expand-env
